﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using agi = HtmlAgilityPack;
namespace WinP
{

    public partial class Form1 : Form
    {
        readonly string[] week = { "mon", "tue", "wed", "thu", "fri", "sat", "sun" };
        string path = "";
        WebClient wc = new WebClient();
        Thread threadDownload;
        Thread threadPrintEpisode;
        public Form1()
        {
            InitializeComponent();
            wc.Encoding = Encoding.UTF8;
            path = "https://comic.naver.com/webtoon/";
            //기본적인 uri주소를 초기화합니다.

            //크로스 스레드 오류를 출력하지 않게 합니다.
            CheckForIllegalCrossThreadCalls = false;
        }

        //요일 콤보박스에서 요일 선택시 호출됩니다.
        private void WeekComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedWeek = week[weekComboBox.SelectedIndex];

            //웹툰목록 리셋
            toonListView.Items.Clear();

            //해당 요일의 웹툰페이지로 이동합니다.
            string html = wc.DownloadString(path + "weekdayList.nhn?week=" + selectedWeek);
            agi.HtmlDocument Doc = new agi.HtmlDocument();
            Doc.LoadHtml(html);

            //웹툰 썸네일을 받을 ImageList를 선언하고 초기화합니다.
            ImageList thumList = new ImageList();
            thumList.ImageSize = new Size(83, 90);
            thumList.ColorDepth = ColorDepth.Depth32Bit;

            //웹툰 썸네일을 위한 인덱스값입니다.
            int toonIndex = 0;

            //printToonList를 스레드로 실행합니다.
            Thread ts = new Thread(() => printToonList(Doc, thumList, toonIndex));
            ts.Start();

            //웹툰 썸네일을 리스트뷰에 등록하고 사이즈를 제조정합니다.
            toonListView.LargeImageList = thumList;
            toonListView.SmallImageList = thumList;
            toonListView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
        }

        void printToonList(agi.HtmlDocument Doc, ImageList thumList, int toonIndex)
        {
            try
            {
                agi.HtmlNodeCollection ulCollection = Doc.DocumentNode.SelectNodes("//ul");
                for (int ul = 0; ul < ulCollection.Count; ul++)
                {

                    //썸네일 이미지를 가지고있는 블록일 경우
                    if (ulCollection[ul].Attributes["class"] != null && ulCollection[ul].Attributes["class"].Value == "img_list")
                    {
                        agi.HtmlNodeCollection liCollection = ulCollection[ul].SelectNodes("li");
                        for (int li = 0; li < liCollection.Count; li++)
                        {
                            string title = "";
                            string writer = "";
                            string toonID = "";
                            agi.HtmlNodeCollection lastCollection = liCollection[li].SelectNodes(".//a");


                            for (int a = 0; a < lastCollection.Count; a++)
                            {

                                //웹툰명을 가져옵니다. 
                                if (lastCollection[a].Attributes["title"] != null && lastCollection[a].Attributes["title"].Value != "")
                                    title = lastCollection[a].Attributes["title"].Value;
                                if (lastCollection[a].Attributes["href"] != null)
                                {

                                    //웹툰의 고유ID값을 가져옵니다.
                                    if (lastCollection[a].Attributes["href"].Value.Contains("titleId"))
                                    {
                                        int idStartIndex = lastCollection[a].Attributes["href"].Value.IndexOf("=") + 1;
                                        int idEndIndex = lastCollection[a].Attributes["href"].Value.IndexOf("&");
                                        toonID = lastCollection[a].Attributes["href"].Value.Substring(idStartIndex, idEndIndex - idStartIndex);
                                    }

                                    //작가명을 가져옵니다.
                                    if (lastCollection[a].Attributes["href"].Value == "#")
                                        writer = lastCollection[a].InnerText;
                                }

                                //웹툰 리스트뷰에 정보를 추가합니다.
                                if (title != "" && writer != "" && ContainItem(toonListView, "웹툰명", title) == null)
                                {
                                    agi.HtmlNode imgNode = liCollection[li].SelectSingleNode(".//img");
                                    string imgPath = imgNode.Attributes["src"].Value;
                                    thumList.Images.Add(LoadImage(imgPath));

                                    ListViewItem toonInfo = new ListViewItem(new string[] { title, writer, toonID });
                                    toonListView.Items.Add(toonInfo);

                                    //웹툰 리스트뷰의 ImageIndex값을 조정해줍니다.(썸네일 이미지를 위함입니다.)
                                    toonListView.Items[toonListView.Items.IndexOf(toonInfo)].ImageIndex = toonIndex++;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e) { }
        }

        //웹툰 리스트뷰에서 웹툰 선택시
        private void ToonListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            int page = 1;
            if (toonListView.SelectedItems.Count > 0)
            {
                //에피소드 리스트뷰를 리셋합니다.
                episodeListView.Items.Clear();

                //에피소드별 이미지를 위한 ImageList를 선언및 초기화합니다.
                ImageList thumList = new ImageList();
                thumList.ImageSize = new Size(71, 41);
                thumList.ColorDepth = ColorDepth.Depth32Bit;

                //에피소드별 index값입니다.
                int num = 0;

                //에피소드를 가져올때, 마지막 페이지라면 false가 됩니다.
                bool keepSearch = true;

                //에피소드별 이미지를 위한 index값입니다.
                int toonIndex = 0;

                agi.HtmlDocument Doc = new agi.HtmlDocument();

                //printEpisode를 스레드로 실행합니다.
                threadPrintEpisode = new Thread(() => printEpisode(keepSearch, page, Doc, thumList, num, toonIndex));
                threadPrintEpisode.Start();
            }

        }
        void printEpisode(bool keepSearch, int page, agi.HtmlDocument Doc, ImageList thumList, int num, int toonIndex)
        {
            try
            {
                //마지막페이지이가 아니라면 계속해서 다음 페이지를 탐색해서 에피소드들의 정보를 탐색합니다.
                while (keepSearch)
                {
                    //웹툰의 고유ID값을 이용해 해당웹툰의 에피소드 페이지에 접속합니다.
                    string html = wc.DownloadString(path + "list.nhn?titleId=" + toonListView.FocusedItem.SubItems[GetColumnsIndex(toonListView, "ID")].Text + "&page=" + page++);
                    Doc.LoadHtml(html);

                    //에피소드가 표시되있는 페이지의 웹툰목록이 table형식으로 되어있습니다.
                    //그 테이블값을 가져옵니다.
                    agi.HtmlNodeCollection tablebody = Doc.DocumentNode.SelectNodes("//table[@class='viewList']");

                    //그 안에 tr이라는 노드를 선택합니다.
                    agi.HtmlNodeCollection trCollection = tablebody[0].SelectNodes("./tr");

                    for (int tr = 0; tr < trCollection.Count; tr++)
                        if (trCollection[tr].Attributes["class"] == null)
                        {
                            string title = "";
                            string date = "";
                            agi.HtmlNodeCollection tdCollection = trCollection[tr].SelectNodes("./td");
                            for (int td = 0; td < tdCollection.Count; td++)
                                if (tdCollection[td].Attributes["class"] != null)
                                {
                                    if (tdCollection[td].Attributes["class"].Value == "title")
                                    {

                                        //해당 에피소드의 제목을 가져옵니다.
                                        agi.HtmlNode aNode = tdCollection[td].SelectSingleNode(".//a");
                                        title = aNode.InnerText;
                                    }

                                    //해당 에피소드가 등록된 날짜를 가져옵니다.
                                    else if (tdCollection[td].Attributes["class"].Value == "num")
                                        date = tdCollection[td].InnerText;
                                }
                            if (title != "" && date != "")

                                //episodeListView의 "날짜"column의 item들 중 date와 같은 값을 가진 항목이 있는지 비교합니다.
                                //날짜가 같은 항목이 없다면
                                if (ContainItem(episodeListView, "날짜", date) == null)
                                {
                                    //에피소드별 대표 이미지 값을 가져와 episodeListView에 에피소드 정보를 등록합니다.
                                    agi.HtmlNode imgNode = trCollection[tr].SelectSingleNode(".//img");
                                    string imgPath = imgNode.Attributes["src"].Value;

                                    //해당 이미지를 thumList에 등록합니다.
                                    thumList.Images.Add(LoadImage(imgPath));

                                    ListViewItem toonInfo = new ListViewItem(new string[] { (num++).ToString(), title, date });
                                    episodeListView.Items.Add(toonInfo);

                                    int cnt = episodeListView.Items.Count;
                                    for (int i = 0; i < episodeListView.Items.Count; i++)
                                        episodeListView.Items[i].SubItems[GetColumnsIndex(episodeListView, "화")].Text = (cnt--).ToString();
                                    episodeListView.Items[episodeListView.Items.IndexOf(toonInfo)].ImageIndex = toonIndex++;
                                    episodeListView.LargeImageList = thumList;
                                    episodeListView.SmallImageList = thumList;
                                    episodeListView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);

                                }

                                //날짜가 같은 항목이 있다면
                                else
                                {
                                    //마지막 페이지 입니다.
                                    //탐색을 종료합니다.
                                    keepSearch = false;
                                    break;
                                }
                        }
                }
            }
            catch (Exception e)
            {
            }
        }

        //이미지의 uri를 통해 Image값을 반환합니다.
        private Image LoadImage(string url)
        {
            WebRequest request = WebRequest.CreateDefault(new Uri(url));
            WebResponse response = request.GetResponse();
            Stream responseStream = response.GetResponseStream();

            Bitmap bmp = new Bitmap(responseStream);

            responseStream.Dispose();
            response.Dispose();

            return bmp;
        }

        //"listView"의 "column"에 해당하는 column에서 "keyword"를 가진 항목을 찾아내어 반환합니다.
        private ListViewItem ContainItem(ListView listView, string column, string keyword)
        {
            for (int i = 0; i < listView.Items.Count; i++)
            {
                int index = GetColumnsIndex(listView, column);
                if (index < 0)
                    return null;
                ListViewItem item = listView.Items[i];
                bool isContains = item.SubItems[index].Text.Contains(keyword);
                if (isContains) { return item; }
            }
            return null;
        }

        //listView의 "name"에 해당하는 column에서의 index값을 반환합니다.
        private int GetColumnsIndex(ListView listView, string name)
        {
            for (int i = 0; i < listView.Columns.Count; i++)
            {
                if (listView.Columns[i].Text == name)
                    return i;
            }
            return -1;
        }

        //경로설정 버튼을 눌렀을 시 호출됩니다.
        private void ButtonSelectPath_Click(object sender, EventArgs e)
        {
            //folderBrowserDialog1를 활성화하고, 경로값을 DownloadPath 텍스트박스에 저장합니다.
            folderBrowserDialog1.ShowDialog();
            if (folderBrowserDialog1.SelectedPath != "")
                DownloadPath.Text = folderBrowserDialog1.SelectedPath;
        }

        //다운로드 버튼을 눌렀을 시 호출됩니다.
        private void ButtonDownload_ClickAsync(object sender, EventArgs e)
        {
            threadDownload = new Thread(() => DownloadThread());
            threadDownload.Start();
        }
        private void DownloadThread()
        {
            agi.HtmlDocument Doc = new agi.HtmlDocument();

            //다운로드중에는 중단버튼을 누르지 않는 이상, 어떠한 선택도 할 수 없습니다.
            var episodeListViewSelectedItems = episodeListView.SelectedItems;
            toonListView.Enabled = false;
            episodeListView.Enabled = false;
            weekComboBox.Enabled = false;

            //전체 진행도를 표시하기위해 totalProgressLabel을 초기화합니다.
            totalProgressLabel.Text = 0 + "/" + episodeListViewSelectedItems.Count;

            for (int i = 0; i < episodeListViewSelectedItems.Count; i++)
            {
                //선택한 웹툰의 ID값과 거기서 선택한 에피소드의 index값을 통해 해당 에피소드의 만화가 수록되어있는 페이지에 접속합니다.
                string id = toonListView.FocusedItem.SubItems[GetColumnsIndex(toonListView, "ID")].Text;
                string episode = episodeListViewSelectedItems[i].SubItems[GetColumnsIndex(episodeListView, "화")].Text;
                string html = wc.DownloadString(path + "detail.nhn?titleId=" + id + "&no=" + episode);
                Doc.LoadHtml(html);

                //만화뷰어 노드의 유무를 확인합니다.
                //만일 없다면 18세이상 관람가능한 웹툰이므로, 로그인 페이지로 넘어갑니다.
                agi.HtmlNode viewerNode = Doc.DocumentNode.SelectSingleNode("//div[@class='wt_viewer']");
                if (viewerNode != null)
                {
                    //만화 image파일을 가지고있는 노드를 선택합니다.
                    agi.HtmlNodeCollection imgCollection = viewerNode.SelectNodes(".//img");

                    //진행도를 표시하기위해 progressBar1을 초기화합니다.
                    progressBar1.Maximum = imgCollection.Count;
                    progressBar1.Value = 0;

                    for (int imgIndex = 0; imgIndex < imgCollection.Count; imgIndex++)
                    {
                        if (DownloadPath.Text != "")
                        {
                            //만화이미지의 uri값을 가져옵니다.
                            string uri = imgCollection[imgIndex].Attributes["src"].Value;

                            //다운로드받는 이미지파일의 uri주소를 로그 텍스트박스에 표시합니다.
                            LogTextBox.Text += uri + "\r\n";

                            //다운로드 받을 경로를 확인하고 폴더가 없다면 생성합니다.
                            string sDirPath = DownloadPath.Text + "\\" + id + "\\" + episode;
                            DirectoryInfo di = new DirectoryInfo(sDirPath);
                            if (di.Exists == false)
                                di.Create();

                            //이미지의 uri값을 이용해 sDirPath의 경로에 "imgIndex".jpg 의 값으로 저장합니다.
                            //만일 URI string이 너무 길게 되면 0byte의 빈 파일을 다운로드받게 됩니다.
                            //그래서 아래와 같은 방법을 적용 하였습니다.
                            DownloadManager downloadManager = new DownloadManager();
                            downloadManager.DownloadFile(uri, sDirPath + "\\" + imgIndex + ".jpg");
                        }
                        else
                        {
                            MessageBox.Show("경로를 설정해 주세요.");
                            return;
                        }
                        //진행바의 진행도값을 올립니다.
                        progressBar1.Value += 1;
                    }
                }
                else
                {
                    MessageBox.Show("18세이상의 웹툰은 불가능합니다.");
                }

                //선택한 에피소드들의 전채 진행도를 표시합니다.
                totalProgressLabel.Text = i + 1 + "/" + episodeListView.SelectedItems.Count;
            }
            //다운로드가 모두 완료되면 다시 웹툰과 에피소드를 선택할 수 있습니다.
            toonListView.Enabled = true;
            episodeListView.Enabled = true;
            weekComboBox.Enabled = true;
        }

        //중단 버튼을 눌렀을때 호출됩니다.
        private void ButtonStop_Click_1(object sender, EventArgs e)
        {
            //다운로드 중일때 다운로드를 중단합니다.
            if (threadDownload != null)
                threadDownload.Abort();

            //에피소드를 탐색중일때 탐색을 중단합니다.
            if (threadPrintEpisode != null)
                threadPrintEpisode.Abort();

            //진행도를 0으로 초기화하고
            //웹툰과 에피소드를 선택할 수 있도록 합니다.
            progressBar1.Value = 0;
            totalProgressLabel.Text = "0/0";
            toonListView.Enabled = true;
            episodeListView.Enabled = true;
            weekComboBox.Enabled = true;
        }

        //검색 텍스트박스에서 키를 땠을때 호출됩니다.
        private void TextBoxSearch_KeyUp(object sender, KeyEventArgs e)
        {
            //엔터 입력시
            if (e.KeyCode == Keys.Return)
            {

                //해당 검색어를 포함한 제목을 가진 웹툰을 선택합니다.
                for (int i = 0; i < toonListView.Items.Count; i++)
                    if (toonListView.Items[i].SubItems[0].Text.Contains(textBoxSearch.Text))
                    {
                        toonListView.Items[i].Selected = true;
                        toonListView.Items[i].Focused = true ;
                        LogTextBox.Text += toonListView.Items[i].SubItems[0].Text;
                    }
            }
        }
    }


    //파일의 uri주소 길이가 길어도 다운로드 받을 수 있는 기능을 가진 class
    public class DownloadManager
    {

        public void DownloadFile(string sourceUrl, string targetFolder)
        {
            WebClient downloader = new WebClient();
            downloader.Headers.Add("User-Agent", "Mozilla/4.0 (compatible; MSIE 8.0)");

            //downloader.DownloadFileCompleted += new AsyncCompletedEventHandler(Downloader_DownloadFileCompleted);
            //downloader.DownloadProgressChanged +=
            //    new DownloadProgressChangedEventHandler(Downloader_DownloadProgressChanged);

            downloader.DownloadFileAsync(new Uri(sourceUrl), targetFolder);
            //while (downloader.IsBusy) { }
        }

        //파일 다운로드 도중 호출됩니다.
        private void Downloader_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {

        }

        //파일 다운로드 완료 후 호출됩니다.
        private void Downloader_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            // display completion status.
            if (e.Error != null)
                Console.WriteLine(e.Error.Message);
            else
                Console.WriteLine("Download Completed!!!");
        }
    }
}
